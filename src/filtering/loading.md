It is generally recommended to mux raw streams (`.h264`, `.h265`, etc.) into containers (such as using [`MKVToolNix`](https://mkvtoolnix.download/)) before loading with source filters.

There are a few source filters that can be used to load videos into VapourSynth. If you have an NVIDIA GPU, [`DGDecNV`](http://rationalqm.us/dgdecnv/dgdecnv.html) is recommended:

```py
src = core.dgdecodenv.DGSource("src.dgi")
```

`.dgi` index file is generated for the source video file using the `DGIndexNV` program in the package.

If you don't have an NVIDIA GPU, [`L-SMASH-Works`](https://github.com/AkarinVS/L-SMASH-Works) is recommended:

```py
src = core.lsmas.LWLibavSource("src.mkv")
```

[`ffms2`](https://github.com/FFMS/ffms2) can also be be used, although certain versions add extra black frames to the beginning of video:

```py
src = core.ffms2.Source("src.mkv")
```

For MPEG2 sources, [`d2vsource`](https://github.com/dwbuiten/d2vsource) and [`DGMPGDec`](http://rationalqm.us/dgmpgdec/dgmpgdec.html) can be used.